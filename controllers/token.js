var Usuario = require('../models/usuarios');
var Token = require('../models/token');


exports.confirmationGet = function (req, res, next) {
    Token.findOne({token:req.params.token},function(err,tokenSuccees){
        if(tokenSuccees==null){
            return res.status(400).send({type:'not-verified',msg:'No se encuentra usuario'})
        }
        Usuario.findOne(tokenSuccees.usuario_Id,function(err,userValidation){
            if(err){
                console.log('No se encuentra usuario',err.message);
                return res.status(400).send({type:'not-found',msg:'No se encuentra usuario'});
            }
            if(userValidation.verificado){
                res.redirect('../../usuarios');
            }
            userValidation.verificado=true;
            userValidation.save(function(err){
                if(err){ return res.status(500).send({msg:err.message}); }
                res.redirect('/');
            })          
        });
    })

};