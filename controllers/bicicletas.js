 var Bicicleta = require('../models/bicicleta');

 exports.bicicleta_list = function (req, res) {
     var data = Bicicleta.allBicis(function (err, bicisResponse) {
         if (err) console.log('eror en la consulta de bicis');
         console.log('ingreso' + bicisResponse);
         return res.render('bicicletas/index', {
             bicis: bicisResponse
         });
     })
 };


 exports.bicicleta_create_get = function (req, res) {
     res.render('bicicletas/create')
 };

 exports.bicicleta_create_post = function (req, res) {
     var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo, [req.body.lat, req.body.lng])
     console.log('labici es', bici);
     Bicicleta.add(bici);
     res.redirect('../bicicletas');
 };

 exports.bicicleta_delete_post = function (req, res) {
     console.log('se elimino', req.body.id)
     Bicicleta.findByIdAndDelete(req.body.id, function (err, response) {
         if (err) {
             console.log('no eliminado')
         } else {
             console.log('eliminado', response)
             return res.redirect('../../bicicletas');
         }
     })

 };

 exports.bicicleta_update_get = function (req, res) {
     var bici = Bicicleta.findById(req.params.id, function (err, bici) {
         if (err) console.log('eror en la consulta de bicis 41');
         console.log('ingreso' + bici);
         return res.render('bicicletas/update', {
             bici
         });
     });
 }

 exports.bicicleta_update_post = function (req, res) {
     console.log('ingreso', req.params.id);
     var biciupdate = {
         code: req.body.code,
         color: req.body.color,
         modelo: req.body.modelo,
         ubicacion: [req.body.lat, req.body.lng]
     }
     console.log('ingreso', biciupdate);
     Bicicleta.findOneAndUpdate(req.params.id, biciupdate, function (err, response) {
         if (err) {
             console.log('Ocurrio un error en la actualizacion linea 42')
         } else {
             console.log('Redireccionar', response)
             res.redirect('../../bicicletas');
         }
     });


 };