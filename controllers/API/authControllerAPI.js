const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');
var Usuarios = require('../../models/usuarios');


exports.authenticate = function(req,res,next){
    Usuarios.findOne({mail:req.body.email},function(err,usuarioResponse){
        if(err){
            next(err);
        }else{
            if(usuarioResponse===null){ return res.status(401).json({status:"error",message:'Invalido email/password', data:null}) }
            if(usuarioResponse!=null && bcrypt.compareSync(req.body.password, usuarioResponse.password)){
                const token = jwt.sign({id: usuarioResponse._id}, req.app.get('secretKey'), { expiresIn:'7d' });
                res.status(200).json({ message:'Usuario encontrado y logeado', data:{usuario: usuarioResponse, token:token }});
                
            }else{
                res.status(401).json({status:"error",message:'Invalido email/password', data:null}) 
            }
        }
    });
};

exports.forgotPassword = function(req,res,next){
    Usuarios.findOne({mail:req.body.email},function(err,usuarioResponse){
        if(err) next(err)
        if(!usuarioResponse){ return res.status(401).json({status:"error",message:'Invalido email/password', data:null}) };
        usuarioResponse.resetPassword(function(err){
            if(err) return next(err);
            res.status(200).json({message:'Se envio un email para restablecer la contraseña', data:null}) 
        });
    });
}

exports.authFacebookToken =function(req,res,next){
    if(req.user){
        req.user.save().then(()=>{
            const token = jwt.sign({id: req.user._id}, req.app.get('secretKey'), { expiresIn:'7d' });
            res.status(200).json({ message:'Usuario encontradoy creado', data:{user: req.user, token:token }});
        }).catch((err)=>{
            console.log('Error en facebook authentication');
            res.status(500).json({message:err.message});
        })
    }else{
        res.status(400)
    }
};
