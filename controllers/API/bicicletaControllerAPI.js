var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req, res) {
    var data = Bicicleta.find({}, function (error, bicis) {
        if (error) {
            console.log("error al consultar");
        } else {
            console.log("se buscaron ", bicis.length);
        }
        res.status(200).json({
            bicicletas: bicis
        });
    })


}

exports.biclicte_create = function (req, res) {
    var aBici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
    aBici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(aBici);
    console.log('API', aBici);

    res.status(200).json({
        bicicletas: aBici
    });
};


exports.biclicte_delete = function (req, res) {
    console.log('API Ingreso', req.body.code);
    Bicicleta.removeByCode(req.body.code, function (error, targetBici) {
        if (error) {
            console.log("error al eliminar");
        } else {
            console.log("Eliminado con", targetBici);
        }
    });
    res.status(204).send();
}

exports.biclicte_update = function (req, res) {
    const bici = Bicicleta.findByCode(req.body.code);
    if (bici !== null) {
        const update = {
            code: req.body.code,
            color: req.body.color,
            modelo: req.body.modelo,
            ubicacion: [req.body.lat, req.body.lng]
        };

        Bicicleta.updateOne({ code: req.body.code }, update, {upsert: true}, (error, biciUpdate) => {
            if (error) {
                console.log('Ocuirrio un error', error);
                res.send(500, {error: error});
            } else {
                console.log('Succesfully saved.', biciUpdate);
                res.status(200).send();
            }
        });
    } else {
        res.status(400).send();
    }
};