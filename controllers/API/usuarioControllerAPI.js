var mongoose = require('mongoose');
var Usuarios = require('../../models/usuarios');


exports.usuarios_list = function (req, res) {
    console.log('ingrese a listado de usuarios')
    Usuarios.find({}, function (err, responseUsuarios) {
        res.status(200).json({
            usuarios: responseUsuarios
        })
    });
};

exports.usuarios_create = function (req, res) {
    var usuario = new Usuarios({
        nombre: req.body.nombre,
        apellido: req.body.apellido
    })
    usuario.save(function (err) {
        res.status(200).json(usuario);
    });
};

exports.usuario_reserva = function (req, res) {
    console.log('ingreso a reservas')
    Usuarios.findById(req.body.id, function (err, usuario) {
        console.log(usuario);
        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function (erro, reserva) {
            console.log("reserva realizada!!1");
            res.status(200).json({
                reserva: "reserva realizada"
            }).send();
        })
    });
}