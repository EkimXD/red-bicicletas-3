var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Reserva = require('./reservas');
var Token = require('./token');
var mailer = require('../mailer/mailer');
var crypto = require('crypto');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt')
const saltRounds = 10; 


const validateEmail = function (mail) {
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return re.test(mail);
}

var usuariosSchema = new Schema({
    nombre: {
        type: String,
        trim: true, 
        required: [true, 'El nombre es obligatorio'] 

    },
    apellido: {
        type: String,
        trim: true, 
        required: [true, 'El apellido es obligatorio']
    },
    mail: {
        type: String,
        trim: true, 
        required: [true, 'El email es obligatorio'],
        lowercase: true, 
        unique: true, 
        validate: [validateEmail, 'Email no valido'],
        match: [/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i] //Criterio de validacion para mongo

    },
    password: {
        type: String,
        required: [true, 'La constraseña es obligatoria']
    },
    passwordResetToken: {
        type: String
    },
    passwordResetTokenExpires: {
        type: Date,
    },
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: {
        type: String
    },
    facebookId: {
        type: String
    }
});

usuariosSchema.plugin(uniqueValidator, {
    message: 'El {PATH} ya existe con otro usuario'
});

usuariosSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuariosSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

usuariosSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde: desde,
        hasta: hasta
    });
    console.log('El objeto se guardo')
    console.log(reserva);
    reserva.save(cb) 
};

usuariosSchema.methods.enviar_email_bienvenida = function (cb) {
    var tokens = new Token({
        usuario_Id: this.id,
        token: crypto.randomBytes(16).toString('hex'),
    });
    var email_destination = this.mail;
    tokens.save(function (err) {
        if (err) {
            return console.log('ocurrio un error al crear', err.message);
        }
        let message = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta ✔',
            text: 'Por favor,para verificar tu cuenta haga click en este link: \n' +
                'http://localhost:5000' + '\/token/confirmacion\/' + tokens.token + '. \n\n'
        };
        mailer.sendMail(message, function (error) {
            if (error) {
                console.log('Error en el envio', error.message);
            }
        });
    });
};


usuariosSchema.methods.resetPassword = function (cb) {
    console.log('el id de la persona es para generar nueva password' + this.id);
    var tokens = new Token({
        usuario_Id: this.id,
        token: crypto.randomBytes(16).toString('hex'),
    });
    var email_destination = this.mail;

    tokens.save(function (err) {
        if (err) {
            console.log('ocurrio un error al crear', err.message);
            return cb(err);
        }

        let message = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reinicio de contraseña en la cuenta',
            text:'Para establecer una nueva contraseña haga click en este link: \n' +
                'http://localhost:5000' + '\/resetPassword\/' + tokens.token + '. \n\n'
        };

        mailer.sendMail(message, function (error) {
            if (error) {
                console.log('Error en el envio', error.message);
            }
            console.log('se envio un correo de nueva contraseña correctamentamente', email_destination);
        });
        cb(null);
    });
};

usuariosSchema.statics.findOneOrCreateByGoogle = function findOneCreate(condition, callback) {
    const self = this;
    console.log('condiction', condition)
    console.log('condiction2', condition.email)
    self.findOne({
        $or: [{
            'googleId': condition.id
        }, {
            'mail': condition.emails[0].value
        }]
    }, (err, result) => {
        if (result) { 
            callback(err, result);
        } else {
            let value = {};
            value.googleId = condition.id
            value.mail = condition.emails[0].value;
            value.nombre = condition.displayName || 'No name';
            value.verificado = true;
            value.password = bcrypt.hashSync(condition.id, saltRounds);
            self.create(value, (err, result) => {
                if (err) {
                    console.log('error al crear' + err)
                }
                callback(err, result);
            })
        }
    })
};

usuariosSchema.statics.findOneOrCreateByFacebook = function findOneCreate(condition, callback) {
    const self = this;
    console.log('condiction FB', condition)
    console.log('condiction FB', condition.emails)
    self.findOne({
        $or: [{
            'facebookId': condition.id
        }, {
            'mail': condition.emails[0].value
        }]
    }, (err, result) => {
        if (result) {
            callback(err, result);
        } else {
            let value = {};
            value.facebookId = condition.id
            value.mail = condition.emails[0].value;
            value.nombre = condition.displayName || 'No name';
            value.verificado = true;
            value.password = bcrypt.hashSync(condition.id, saltRounds);
            self.create(value, (err, result) => {
                if (err) {
                    console.log('error al crear' + err)
                }
                callback(err, result);
            })
        }
    })
};


module.exports = mongoose.model('Usuarios', usuariosSchema);