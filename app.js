'use strict'; 
require('newrelic')
var sessionstorage = require('sessionstorage');
require('dotenv').config(); 
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const session = require('express-session');
const passport = require('./config/passport')
const Usuarios = require('./models/usuarios');
const Token    = require('./models/token');
const mongoDBStore=require('connect-mongodb-session')(session);

var jwt = require('jsonwebtoken');
var mongoose = require('mongoose');
let store
if(process.env.NODE_ENV ==='development'){
  var mongoDB = process.env.MONGO_URI;
  store = new session.MemoryStore;
}else{
  var mongoDB = process.env.MONGO_URI_PRD;
  store = new mongoDBStore({
    uri: mongoDB,
    collection:'sessions'
  });
  store.on('error',function(error){
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();
app.set('secretKey','jwt_p23344');
app.use(session({
  cookie: {
    maxAge: 240 * 60 * 60 * 1000
  }, 
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bici123' 
}));

mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'mongoDB conection error'));
db.once('open', () => {});

var indexRouter = require('./routes/index');
var bicicletasRouter = require('./routes/bicicletas');
var usuariosRouter = require('./routes/usuariosRoutes');
var tokenRouter = require('./routes/tokenRouter');
var loginRouter = require('./routes/loginRouter');
var registerRouter = require('./routes/register');

var authApiRouter = require('./routes/API/authRouter');
var bicicletasRouterAPI = require('./routes/API/bicicletasRouter');
var usuarioRouterAPI = require('./routes/API/usuarioRouter');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.post('/login', function (req, res, next) {
  passport.authenticate('local', function (err, usuario, info) {
    if (err) return next(err);
    if (!usuario) return res.render('session/login', {
      info
    });
    req.logIn(usuario, function (err) {
      if (err) return next(err);
      const token = jwt.sign({id: usuario._id}, req.app.get('secretKey'), { expiresIn:'7d' });     
      console.log('Token generado es ',token)    
     return res.redirect('/any/'+token);
    });
  })(req, res, next);
});

app.get('/logout', function (req, res, next) {
  req.logOut();
  res.redirect('/out');
});

app.get('/forgotPassword',function(req, res){
  res.render('session/forgotPassword');
})

app.post('/forgotPassword', function (req, res, next) { 
  Usuarios.findOne({ mail: req.body.email }, function (err, usuario) {    
    if (err) return next(err);
    if (usuario===null) return res.render('session/forgotPassword', {
      info: {
        message: 'No existe el email para el usuario ingresado'
      }});
   
    usuario.resetPassword(function(err){
        if(err)return next(err);
        console.log('Se envio a ','/session/forgotPasswordMessage');
    })
    res.render('session/forgotPasswordMessage');

  });
});

app.get('/resetPassword/:token', function (req, res, next) {
  console.log('el token es '+req.params.token)
  Token.findOne({token:req.params.token},(err,token)=>{
      if(err) res.status(400).send({type:'not-verified',message:'No existe usuario asociado al token.Verifique que su token no haya expirado'});

      Usuarios.findById(token.usuario_Id,function(err,usuario){
        if(usuario===null){
          res.status(400).send({message:'No existe un usuario asociado al token'});          
        }
        res.render('session/resetPassword',{errors:{},usuario : usuario});
      });
  });   
});

app.post('/resetPassword', function (req, res, next) {
  console.log('el body es',req.body);
  if(req.body.pwd!==req.body.confirm_pwd){
    res.render('session/resetPassword',
    {
      errors  : { confirm_pwd: { message:'No coinciden con el password ingresado' } },
      usuario : new Usuarios({mail:req.body.mail})
    });
  return;
  }
  Usuarios.findOne({ mail: req.body.email }, function (err, usuario) {  
    console.log('usuario encontrado',usuario);
    if(err){
      console.log('error en la consulta del cliente');
    }else{
      if(usuario.password!==null){
        usuario.password=req.body.pwd
        usuario.save(function(err){
          if(err){
            res.render('session/resetPassword',
            {
              info    : { message:err.errors },
              usuario : new Usuarios({mail:req.body.mail})
            });
          }else{
            res.redirect('/login')
          }
        });
      }
    }
  })
});

function loggedIn(req,res,next){
  if(req.user){
    console.log('usuaria logeado');
    next(); 
  }
  else{
    console.log('usuaria no logeado');
    res.redirect('login')
  }
};
function validationUsuario(req,res,next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'),function(err,decoded){
    if(err){
      res.json({ status:"error",message:err.message, data:null });
    }else{
      req.body.userIdToken=decoded.id;
      console.log('Usuario logeado en api ',decoded);
      next();
    }
  });

}
app.get('/auth/google',
  passport.authenticate('google', { scope: 
      [ 'https://www.googleapis.com/auth/plus.login',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email' ] } 
));

app.get('/auth/google/callback', passport.authenticate( 'google', { failureRedirect: '/error'}),
  function(req, res) {
    res.redirect('/');
});

app.get('/auth/facebook', passport.authenticate('facebook',{
  authType: 'rerequest',
  scope: ['user_friends', 'email', 'public_profile'],
}));

app.get('/auth/facebook/callback',passport.authenticate('facebook',
  { successRedirect: '/',failureRedirect: '/login' })
);


app.use('/', indexRouter);
app.use('/bicicletas',loggedIn, bicicletasRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter)
app.use('/login', loginRouter)
app.use('/register', registerRouter);

app.use('/privacy_policy',function(req,res){
  res.sendFile('public/privacy_policy.html')
});
app.use('/google29f52070ac14cb1c',function(req,res){
  res.sendFile('public/google29f52070ac14cb1c.html')
})

app.use('/api/auth',authApiRouter);
app.use('/api/bicicletas',validationUsuario, bicicletasRouterAPI);
app.use('/api/usuarios', usuarioRouterAPI);

app.use(function (req, res, next) {
  next(createError(404));
});

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;