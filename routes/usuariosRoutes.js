var express = require('express');
var router = express.Router();
var usuariosContoller = require('../controllers/usuarios')

router.get('/', usuariosContoller.list); 
router.get('/create', usuariosContoller.create_get); 
router.post('/create', usuariosContoller.create); 
router.get('/:id/update', usuariosContoller.update_get); 
router.post('/:id/update', usuariosContoller.update);
router.post('/:id/delete', usuariosContoller.delete);

module.exports = router;