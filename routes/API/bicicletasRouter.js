var express = require('express');
var router = express.Router();
var bicicletasController = require('../../controllers/API/bicicletaControllerAPI'); 

router.get('/', bicicletasController.bicicleta_list);
router.post('/create', bicicletasController.biclicte_create);
router.delete('/delete', bicicletasController.biclicte_delete);
router.put('/update', bicicletasController.biclicte_update);
 
module.exports = router;