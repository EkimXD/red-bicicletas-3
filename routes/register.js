var express = require('express');
var router = express.Router();
var controllerRegister = require('./../controllers/register');

router.get('/', controllerRegister.register_get);

module.exports = router;