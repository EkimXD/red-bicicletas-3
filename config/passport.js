const passport = require('passport');
const LocalStrategy = require('passport-local');
const Usuario = require('./../models/usuarios');
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var FacebookTokenStrategyToken = require('passport-facebook-token');
var FacebookStrategy = require('passport-facebook').Strategy;

passport.use(new FacebookTokenStrategyToken({
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret: process.env.FACEBOOK_APP_SECRET   
}, function(accessToken, refreshToken, profile, done) {
    try{
        Usuario.findOneOrCreateByFacebook(profile,(err,user)=>{
            if(err) console.log('Error logeo facebook',err);
            return done(err,user);
        });
    }catch(erroFB){
        console.log("se genero un error en Facebook TOKEN"+erroFB);
        return done(erroFB,null);
    }
  }
));

passport.use(new LocalStrategy(
    function (email, password ,done) {
        Usuario.findOne({mail:email},function(err, usuario){
            if(err) return done(err);
            if(!usuario) return done(null,false,{message:'Email no existe o es incorrecto'});
            if(!usuario.validPassword(password))return done(null,false,{message:'Password incorrecto'});
            //validar si esta  validado el usuario
            return done(null,usuario);
        });
    }
));

passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL : process.env.HOST+'/auth/google/callback'
},function(request, accessToken, refreshToken, profile, cb){
    console.log('Profile de estrategia'+profile)
    Usuario.findOneOrCreateByGoogle(profile,(err,user)=>{
        return cb(err,user);
    })
}));

passport.serializeUser(function (user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    Usuario.findById(id, function (err, usuario) {
        cb(err, usuario);
    });
});

module.exports = passport;