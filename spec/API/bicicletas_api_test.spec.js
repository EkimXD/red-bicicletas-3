var mongoose = require('mongoose');
var Bicicletas = require('../../models/bicicleta')
var serve = require('../../bin/www');
var request = require('request');

var base_url = 'http://localhost:5000/api/bicicletas'


describe('Bicicleta API', () => {

    beforeEach(function (done) {
        var mongoDB = 'mongodb://localhost/testdbbiciletas';
        mongoose.connect(mongoDB, {
            useNewUrlParser: true
        });

        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'mongoDB conection error'));
        db.once('open', () => {
            console.log('MongoDB are conected to test database')
            done();
        });
    });

    afterEach(function (done) {
        Bicicletas.deleteMany({}, function (err, success) {
            if (err !== null) {
                console.log(err);
            }
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, (error, response, body) => {
                var result = JSON.parse(body)
                console.log('en el result', result);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });

        })
    });

    describe('CREATE BICICLETA /create', () => {
        it('Status 200', (done) => {
            var headers = {
                'Content-Type': 'application/json'
            };
            var bici = '{"code": 10,"color": "amarillo", "modelo": "Montaña", "lat": 4.6, "lng": -74.1 }';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: bici
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done(); 
            });
        });
    });


    describe('UPDATE BICICLETA /update', () => {
        it('Status 200', (done) => {
            var headers = {
                'Content-Type': 'application/json'
            };
            var bici = '{"code": 10,"color": "azul", "modelo": "Montaña", "lat": 4.6417424, "lng": -74.1301659 }';
            request.put({
                headers: headers,
                url: base_url + '/update',
                body: bici
            }, (error, response, body) => {
                expect(response.statusCode).toBe(200);
                done(); 
            });
        });
    });

    describe('UPDATE BICICLETA /delete', () => {
        it('Status 200', (done) => {
            var headers = {
                'Content-Type': 'application/json'
            };
            var bici = '{"code": 10 }';
            request.delete({
                headers: headers,
                url: base_url + '/delete',
                body: bici
            }, (error, response, body) => {
                expect(response.statusCode).toBe(204);
                expect(Bicicletas.allBicis.length).toBe(1);
                done();
            });
        });
    });


});